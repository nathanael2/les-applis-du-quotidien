---
title: "Ma page de recommandations"
order: 0
in_menu: true
---
Voici les quelquels applications et logiciels que j'utilise au quotidien et qui sont très pratiques !

# Pour discuter : 

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Signal.png">
    </div>
    <div>
      <h2>Signal</h2>
      <p>Application de messagerie et téléphonie mobile respectueuse de la vie privée.</p>
      <div>
        <a href="https://framalibre.org/notices/signal.html">Vers la notice Framalibre</a>
        <a href="https://signal.org">Vers le site</a>
      </div>
    </div>
  </article>


  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Element.io%20(ex%20Riot.im).png">
    </div>
    <div>
      <h2>Element (ex Riot)</h2>
      <p>Element rassemble toutes vos conversations et intégrations applicatives en une seule application.</p>
      <div>
        <a href="https://framalibre.org/notices/element-ex-riot.html">Vers la notice Framalibre</a>
        <a href="https://element.io">Vers le site</a>
      </div>
    </div>
  </article>

# Des mots de passe costauds 💪 et toujours dispo : 


  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Bitwarden.png">
    </div>
    <div>
      <h2>Bitwarden</h2>
      <p>Résolvez tous vos problèmes de gestion de mots de passe. Une solution  simple et sécurisée.</p>
      <div>
        <a href="https://framalibre.org/notices/bitwarden.html">Vers la notice Framalibre</a>
        <a href="https://bitwarden.com/">Vers le site</a>
      </div>
    </div>
  </article>

# Nextcloud, pour plein de choses mais pas le café ☕

- Cloud de fichiers
- Envoi et classement (auto) des photos de mon téléphone
- Contacts synchronisés
- Calendriers synchronisés
- Collections de traces GPS des randos et sorties vélo
- Suivi GPS de mon tél (au cas où je le perdrais)
- ...


  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Nextcloud.png">
    </div>
    <div>
      <h2>Nextcloud</h2>
      <p>Nextcloud est une plate-forme auto-hébergée de services de stockage et d’applications diverses dans les nuages.</p>
      <div>
        <a href="https://framalibre.org/notices/nextcloud.html">Vers la notice Framalibre</a>
        <a href="https://nextcloud.com/">Vers le site</a>
      </div>
    </div>
  </article> 